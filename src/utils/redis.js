const redis = require("redis");

const logger = require("./logger");

const client = redis
    .createClient(process.env.REDIS_PORT, process.env.REDIS_HOST)
    .on("error", err => logger.error(err.message, { err }));

module.exports.shutdown = async function shutdown() {
    if (client && client.connected) {
        client.quit(err => {
            if (err) {
                logger.error(err.message, { err });
                throw err;
            }
        });
    }
};

module.exports.get = async function get(key) {
    return new Promise((resolve, reject) => client.get(key, (err, value) => (err ? reject(err) : resolve(value))));
};

module.exports.set = async function set(key, value) {
    return new Promise((resolve, reject) => client.set(key, value, err => (err ? reject(err) : resolve(value))));
};

module.exports.del = async function del(key) {
    return new Promise((resolve, reject) => client.del(key, err => (err ? reject(err) : resolve())));
};
