const pg = require("pg");

const connectionName = process.env.INSTANCE_CONNECTION_NAME;
const dbUser = process.env.SQL_USER;
const dbPassword = process.env.SQL_PASSWORD;
const dbName = process.env.SQL_NAME;

const pgConfig = {
    max: 1,
    user: dbUser,
    password: dbPassword,
    database: dbName,
    host: `/cloudsql/${connectionName}`
};

module.exports.pool = new pg.Pool(pgConfig);

module.exports.shutdown = async function() {
    await module.exports.pool.end();
};
