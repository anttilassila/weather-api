const municipalities = require("finland-municipalities");

module.exports.cityExists = async function cityExists(city) {
    city = city.toLowerCase();

    for (index in municipalities) {
        if (municipalities[index].KUNTANIMIFI.toLowerCase() == city) {
            return city;
        }
    }

    return null;
};
