const jwt = require("jsonwebtoken");

const redis = require("../utils/redis");
const userService = require("../services/user");

module.exports.getSessionByToken = async function getSessionByToken(tokenFromReq) {
    const { id: userId } = jwt.verify(tokenFromReq, process.env.JWT_SECRET_KEY);
    const user = await userService.getUserById(userId);
    const token = await redis.get(userId);
    if (!token) {
        throw new Error("Token not found in Session Storage!");
    }
    return {
        user,
        token
    };
};

module.exports.createSession = async function createSession(user) {
    const token = jwt.sign({ id: user.id }, process.env.JWT_SECRET_KEY);
    await redis.set(user.id, token);
    return {
        user,
        token
    };
};

module.exports.removeSession = async function removeSession(user) {
    await redis.del(user.id);
};
