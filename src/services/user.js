const boom = require("boom");
const bcrypt = require("bcrypt");

const db = require("../utils/db");
const logger = require("../utils/logger");

/**
 * @todo Make sure the password is hashed with something!
 */
module.exports.createUser = async function createUser(credentials) {
    let client;
    try {
        client = await db.pool.connect();
        console.log("lorem");
        const result = await client.query("SELECT id FROM users u WHERE u.username = $1", [credentials.username]);
        if (result.rows.length > 0) {
            throw boom.conflict("User with that name already exists.");
        }
        const insertResult = await client.query(
            "INSERT INTO users(username, password) VALUES($1, $2) RETURNING id, username",
            [credentials.username, await bcrypt.hash(credentials.password, 8)]
        );
        client.release();
        return insertResult.rows[0];
    } catch (err) {
        if (client) {
            client.release();
        }
        logger.error(err.message, { err });
        throw err;
    }
};

module.exports.getUserById = async function getUserById(id) {
    let client;
    try {
        client = await db.pool.connect();
        const result = await client.query("SELECT id, username FROM users u WHERE u.id = $1", [id]);
        if (result.rows.length) {
            client.release();
            return result.rows[0];
        }
        throw boom.notFound();
    } catch (err) {
        if (client) {
            client.release();
        }
        logger.error(err.message, { err });
        throw err;
    }
};

/**
 * @todo Since the password _should_ be hashed with something, it should also
 *       be compared to the hash.
 */
module.exports.getUserByCredentials = async function getUserByCredentials(credentials) {
    let client;
    try {
        client = await db.pool.connect();
        const result = await client.query("SELECT * FROM users u WHERE u.username = $1", [credentials.username]);
        if (result.rows.length > 0) {
            const user = result.rows[0];
            if (await bcrypt.compare(credentials.password, user.password)) {
                client.release();
                return { id: user.id, username: user.username };
            }
            throw boom.unauthorized("Couldn't match password!");
        }
        throw boom.notFound();
    } catch (err) {
        if (client) {
            client.release();
        }
        logger.error(err.message, { err });
        throw err;
    }
};
