const boom = require("boom");

const db = require("../utils/db");
const logger = require("../utils/logger");
const cities = require("../utils/cities");

module.exports.getFavorites = async function getFavorites(user) {
    let client;
    try {
        client = await db.pool.connect();
        const result = await client.query("SELECT * FROM favorites f WHERE f.owner_id = $1", [user.id]);
        client.release();
        return result.rows;
    } catch (err) {
        if (client) {
            client.release();
        }
        logger.error(err.message, { err });
        throw err;
    }
};

module.exports.createFavorite = async function createFavorite(city, user) {
    city = await cities.cityExists(city.name);

    if (city === null) {
        throw boom.notFound("Location not found!");
    }

    let client;
    try {
        client = await db.pool.connect();
        const result = await client.query("INSERT INTO favorites(name, owner_id) VALUES($1, $2) RETURNING *", [
            city,
            user.id
        ]);
        if (result.rows.length > 0) {
            client.release();
            return result.rows[0];
        }
        throw boom.internal("Something bad happened!");
    } catch (err) {
        if (client) {
            client.release();
        }
        logger.error(err.message, { err });
        throw err;
    }
};

module.exports.removeFavorite = async function removeFavorite(id, user) {
    let client;
    try {
        client = await db.pool.connect();
        const result = await client.query("DELETE FROM favorites WHERE id = $1 AND owner_id = $2", [id, user.id]);
        if (result.rowCount === 0) {
            throw boom.notFound();
        }
        client.release();
    } catch (err) {
        if (client) {
            client.release();
        }
        logger.error(err.message, { err });
        throw err;
    }
};
