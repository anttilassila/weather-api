const axios = require("axios");
const Immutable = require("immutable");
const convert = require("xml-js");
const cities = require("../utils/cities");
const boom = require("boom");
const logger = require("../utils/logger");

module.exports.getWeatherByCity = async function getWeatherByCity(city) {
    try {
        city = await cities.cityExists(city);
        if (city !== null) {
            city = encodeURIComponent(city);
            return axios({
                url: `https://data.fmi.fi/fmi-apikey/${
                    process.env.FMI_API_KEY
                }/wfs?request=getFeature&storedquery_id=fmi::forecast::hirlam::surface::point::simple&place=${city}`,
                method: "GET",
                responseType: "text"
            }).then(apiRequest => {
                const xml = apiRequest.data;
                const result = convert.xml2json(xml, { compact: true });
                const jsonData = JSON.parse(result);
                const IData = Immutable.fromJS(jsonData);
                const members = IData.getIn(["wfs:FeatureCollection", "wfs:member"]);

                let timebased = Immutable.Map();
                members.forEach(elem => {
                    const time = elem.getIn(["BsWfs:BsWfsElement", "BsWfs:Time", "_text"]);
                    const param = elem.getIn(["BsWfs:BsWfsElement", "BsWfs:ParameterName", "_text"]);
                    const value = elem.getIn(["BsWfs:BsWfsElement", "BsWfs:ParameterValue", "_text"]);
                    const timeElem = timebased.get(time, Immutable.Map({ time: time }));
                    timebased = timebased.set(time, timeElem.set(param, value));
                });
                return timebased.sortBy((v, k) => new Date(k));
            });
        } else {
            throw boom.notFound("Location not found!");
        }
    } catch (err) {
        logger.error(err.message, { err });
        throw err;
    }
};
