const boom = require("boom");

const db = require("../utils/db");
const logger = require("../utils/logger");

module.exports.getCats = async function getCats(user) {
  let client;
  try {
    client = await db.pool.connect();
    const result = await client.query(
      "SELECT * FROM cats c WHERE c.owner_id = $1",
      [user.id]
    );
    client.release();
    return result.rows;
  } catch (err) {
    if (client) {
      client.release();
    }
    logger.error(err.message, { err });
    throw err;
  }
};

module.exports.getCatById = async function getCatById(id, user) {
  let client;
  try {
    client = await db.pool.connect();
    const result = await client.query(
      "SELECT * FROM cats c WHERE c.id = $1 AND c.owner_id = $2",
      [id, user.id]
    );
    if (result.rows.length > 0) {
      client.release();
      return result.rows[0];
    }
    throw boom.notFound();
  } catch (err) {
    if (client) {
      client.release();
    }
    logger.error(err.message, { err });
    throw err;
  }
};

module.exports.createCat = async function createCat(cat, user) {
  let client;
  try {
    client = await db.pool.connect();
    const result = await client.query(
      "INSERT INTO cats(name, owner_id) VALUES($1, $2) RETURNING *",
      [cat.name, user.id]
    );
    if (result.rows.length > 0) {
      client.release();
      return result.rows[0];
    }
    throw boom.internal("Something bad happened!");
  } catch (err) {
    if (client) {
      client.release();
    }
    logger.error(err.message, { err });
    throw err;
  }
};

module.exports.updateCat = async function updateCat(cat, user) {
  let client;
  try {
    client = await db.pool.connect();
    const result = await client.query(
      "UPDATE cats SET name = $1 WHERE owner_id = $2 RETURNING *",
      [cat.name, user.id]
    );
    if (result.rows.length > 0) {
      client.release();
      return result.rows[0];
    }
    throw boom.notFound();
  } catch (err) {
    if (client) {
      client.release();
    }
    logger.error(err.message, { err });
    throw err;
  }
};

module.exports.removeCat = async function removeCat(id, user) {
  let client;
  try {
    client = await db.pool.connect();
    const result = await client.query(
      "DELETE FROM cats WHERE id = $1 AND owner_id = $2",
      [id, user.id]
    );
    if (result.rowCount === 0) {
      throw boom.notFound();
    }
    client.release();
  } catch (err) {
    if (client) {
      client.release();
    }
    logger.error(err.message, { err });
    throw err;
  }
};
