const express = require("express");
const morgan = require("morgan");

const router = require("./router");
const logger = require("./utils/logger");
const errorHandler = require("./middleware/error-handler");

module.exports = express()
  .use(
    morgan("short", {
      stream: { write: msg => logger.info(msg.replace("\n", "")) }
    })
  )
  .use(router)
  .use(errorHandler);
