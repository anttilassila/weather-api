const boom = require("boom");

const logger = require("../utils/logger");
const sessionService = require("../services/session");

function getTokenFromRequest(req) {
  const authorization = req.get("Authorization");
  if (authorization && authorization.length > 0) {
    return authorization.replace("Bearer", "").trim();
  }
}

module.exports = async (req, res, next) => {
  let session;
  try {
    session = await sessionService.getSessionByToken(getTokenFromRequest(req));
  } catch (err) {
    logger.error(err.message, { err });
    return next(boom.unauthorized("Could not retrieve token."));
  }
  if (session) {
    req.user = session.user;
    return next();
  }
  return next(boom.unauthorized("Session doesn't exist."));
};
