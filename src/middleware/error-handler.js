const boom = require("boom");
const celebrate = require("celebrate");

const logger = require("../utils/logger");

// eslint-disable-next-line no-unused-vars
module.exports = (err, req, res, next) => {
  if (celebrate.isCelebrate(err)) {
    err = boom.boomify(err, { statusCode: 400 });
  }
  if (!boom.isBoom(err)) {
    err = boom.boomify(err, { statusCode: 500 });
  }
  logger.error(err.message, { err });
  res.status(err.output.statusCode).json(err.output.payload);
};
