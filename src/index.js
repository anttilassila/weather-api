require("dotenv/config");

const app = require("./app");

const db = require("./utils/db");
const redis = require("./utils/redis");
const logger = require("./utils/logger");

process.on("SIGTERM", handleTermination);
process.on("uncaughtException", handleUncaughtException);
process.on("unhandledRejection", handleUncaughtException);

const server = app.listen(process.env.PORT, err => {
    if (err) {
        logger.error(err.message, { err });
        return process.exit(1);
    }
    logger.info("Server listening.", { address: server.address() });
});

/**
 * Handle process termination.
 */
async function handleTermination() {
    try {
        await shutdown();
    } catch (err) {
        logger.error(err.message, { err });
        process.exit(1);
    }
}

/**
 * Handle uncaught exceptions & rejections.
 */
async function handleUncaughtException(err) {
    logger.error(err.message, { err });
    try {
        await shutdown();
    } catch (err) {
        logger.error(err.message, { err });
    }
    process.exit(1);
}

/**
 * Close down the server instance and any other resources used by the
 * application.
 */
async function shutdown() {
    await new Promise((resolve, reject) =>
        server && server.listening ? server.close(err => (err ? reject(err) : resolve())) : resolve()
    );
    await db.shutdown();
    await redis.shutdown();
}
