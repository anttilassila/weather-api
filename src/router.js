const boom = require("boom");
const parser = require("body-parser");
const express = require("express");

const { celebrate, Joi: joi } = require("celebrate");

const authenticate = require("./middleware/authenticate");
const favoriteService = require("./services/favorite");
const sessionService = require("./services/session");
const userService = require("./services/user");
const weatherService = require("./services/weather");

module.exports = express
    .Router()
    .use(parser.json())
    .get(
        "/weather/:city",
        celebrate({
            params: {
                city: joi.string().required()
            }
        }),
        async (req, res, next) => {
            try {
                const weather = await weatherService.getWeatherByCity(req.params.city);
                res.status(200).json(weather);
            } catch (err) {
                next(err);
            }
        }
    )
    .get("/favorites", authenticate, async (req, res, next) => {
        try {
            const favorites = await favoriteService.getFavorites(req.user);
            res.status(200).json(favorites);
        } catch (err) {
            next(err);
        }
    })
    .post(
        "/favorites",
        authenticate,
        celebrate({
            body: {
                name: joi
                    .string()
                    .min(2)
                    .required()
            }
        }),
        async (req, res, next) => {
            try {
                const favorite = await favoriteService.createFavorite(req.body, req.user);
                res.status(201).json(favorite);
            } catch (err) {
                next(err);
            }
        }
    )
    .delete(
        "/favorites/:id",
        authenticate,
        celebrate({
            params: {
                id: joi
                    .number()
                    .integer()
                    .required()
            }
        }),
        async (req, res, next) => {
            try {
                await favoriteService.removeFavorite(req.params.id, req.user);
                res.status(204).send();
            } catch (err) {
                next(err);
            }
        }
    )
    .post(
        "/login",
        celebrate({
            body: {
                username: joi.string().required(),
                password: joi.string().required()
            }
        }),
        async (req, res, next) => {
            try {
                const user = await userService.getUserByCredentials(req.body);
                if (!user) {
                    throw boom.unauthorized("User not found.");
                }

                const session = await sessionService.createSession(user);
                res.status(200).json(session);
            } catch (err) {
                next(err);
            }
        }
    )
    .post("/logout", authenticate, async (req, res, next) => {
        try {
            await sessionService.removeSession(req.user);
            res.status(204).send();
        } catch (err) {
            next(err);
        }
    })
    .post(
        "/register",
        celebrate({
            body: {
                username: joi
                    .string()
                    .min(3)
                    .max(8)
                    .required(),
                password: joi
                    .string()
                    .min(8)
                    .required()
            }
        }),
        async (req, res, next) => {
            try {
                const user = await userService.createUser(req.body);
                res.status(201).json(user);
            } catch (err) {
                next(err);
            }
        }
    );
