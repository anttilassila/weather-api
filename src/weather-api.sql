DROP TABLE IF EXISTS users;
CREATE TABLE IF NOT EXISTS users (
  id       serial       PRIMARY KEY,
  username varchar(128) NOT NULL,
  password varchar(512) NOT NULL
);

DROP TABLE IF EXISTS favorites;
CREATE TABLE IF NOT EXISTS favorites (
  id       serial      PRIMARY KEY,
  name     varchar(64) NOT NULL,
  owner_id integer     REFERENCES users(id)
);
