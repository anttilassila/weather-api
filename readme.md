#Weather API#

##Assignment TODO##

-   RESTful naming conventions for endpoints
-   User can search for the forecast given a municipality (e.g. "Helsinki", "Jyväskylä" etc.)
-   Validate municipalities
-   User can store these municipalities in his/her favorites.
-   User can retrieve a list of his/her favorite municipalities.
-   User can delete a municipality from his/her favorites.
-   Authentication & Authorization
-   User can create a new account
-   The application must provide both a login- and a logout-endpoin
-   User can obtain an access token in exchange for his/her credentials. (JWT token implementation)
-   User can revoke his/her current access token.
-   The application must also have per-user authorization for the stored municipalities:
-   User cannot retrieve another user's stored municipalities.
-   User cannot delete another user's stored municipalities.

##Instructions##

App is running on a Google Cloud App Engine, so you can't start checking it right away.

**1. Checking the weather for a location**

https://yamk-ytsp0200.appspot.com/weather/jyväskylä
https://yamk-ytsp0200.appspot.com/weather/olematonkaupunki

**2. Registering user**

curl -d '{"username":"antti", "password":"Lutakko1"}' -H "Content-Type: application/json" -X POST https://yamk-ytsp0200.appspot.com/register

**3. Login**

curl -d '{"username":"antti", "password":"Lutakko1"}' -H "Content-Type: application/json" -X POST https://yamk-ytsp0200.appspot.com/login

**4. Add favorite locations**

curl -H "Authorization: Bearer YOUR_SESSION_TOKEN_HERE" -d '{"name": "Tampere"}' -H "Content-Type: application/json" -X POST https://yamk-ytsp0200.appspot.com/favorites

**5. Get Favorite locations**

curl -H "Authorization: Bearer YOUR_SESSION_TOKEN_HERE" -X GET https://yamk-ytsp0200.appspot.com/favorites

**6. Delete Favorite location**

curl -H "Authorization: Bearer YOUR_SESSION_TOKEN_HERE" -X DELETE https://yamk-ytsp0200.appspot.com/favorites/ID_NUMBER_HERE

**7. Logout**

curl -H "Authorization: Bearer YOUR_SESSION_TOKEN_HERE" -d '{}' -H "Content-Type: application/json" -X POST https://yamk-ytsp0200.appspot.com/logout

##Time Tracking##

-   26.1.2019 - Getting to know the assignment, Bitbucket repository, project base - 1 h
-   26.1.2019 - Coding the sample app in the class room (cat shop) - 2 h
-   8.3.2019 - Coding the sample app in the class room (cat shop) - 3 h
-   9.3.2019 - Coding the sample app in the class room (cat shop) - 5 h
-   4.5.2019 - Refactoring sample app to weather app - 3 h
-   6.5.2019 - Setting up the Google Cloud App Engine - 2 h
-   7.5.2019 - Deploying app to Google Cloud and finalizing the assignment - 1 h

##Feedback on the assignment##

-   It was rather easy to complete the assignment based on the sample app that was coded in the class room. I decided to test Google Cloud App Engine because Docker is not so nice to be used when one is having a windows machine (with home edition). App Engine seemed pretty easy and fast to get started with, but in reality was bit different. Need for different Cloud services (Redis, SQL) made App Engine startup actually quite complicated. Google has a good documentation but it is not perfect when it comes to combining different services.
